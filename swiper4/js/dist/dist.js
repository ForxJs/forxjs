$(function () {
  $('.ding').click(function () {
    $('.msg').addClass('active');
  });
  $('.msg').click(function () {
    $('.msg').removeClass('active');
  });
  var my = new Swiper('.swiper-container', {
    direction: 'vertical',
    // effect: 'fade',
    fadeEffect: {
      crossFade: true,
    },
    loop: false,
    // 如果需要分页器
    pagination: {
      el: '.swiper-pagination',
    },
    on: {
      slideChangeTransitionStart: function () {
        var py = this.activeIndex * 100 / $('.stage .swiper-slide.card').length + '%';
        $('.stage').css({
          'background-position-y': py
        });
        $('.num').text('')
      },
      init: function () {
        myPage.changeNum(this)
      },
      slideChangeTransitionEnd: function () {
        myPage.changeNum(this)
      }
    },
    // 如果需要前进后退按钮
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },

    // 如果需要滚动条
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
  })
});
// 数字渐变

var myPage = {
  nowindex: null,
  oldIndex: null,
  isChange: false,
  user: {},
  changeNum: function (swiper) {
    $('.card' + (swiper.activeIndex + 1) + ' .num').each(function () {
      var newVal = $(this).attr('data-to');
      var oldVal = $(this).attr('data-from')
      var fixed = $(this).attr('data-fixed')
      myPage.numShade(newVal, oldVal, $(this), fixed);
    })
  },
  numShade: function (newValue, oldValue, jq, fixed) {
    // var vm = this
    jq.text('')

    function animate() {
      if (TWEEN.update()) {
        requestAnimationFrame(animate)
      }
    }
    var o = parseFloat(oldValue),
      n = parseFloat(newValue),
      z = 0,
      isF = false;
    if (newValue.indexOf('.') === -1) {
      z = 0;
      isF = false
    } else {
      z = newValue.split('.')[1].length;
      isF = true
    }
    var d = 1,
      zz = z; //倍数
    while (z > 0) {
      d *= 10;
      z--;
    }
    o *= d;

    new TWEEN.Tween({
        tweeningNumber: o
      })
      .easing(TWEEN.Easing.Quadratic.Out)
      .to({
        tweeningNumber: n * d
      }, 4000)
      .onUpdate(function () {
        var nowVal = this.tweeningNumber;
        jq.text((nowVal / d).toFixed(zz));
      })
      .start()

    animate()
  }
}
var canvas = document.getElementById('gamePlan');
var ctx = canvas.getContext('2d')
var atlas = {
  bird1: [174, 982, 34, 25],
  bird2: [229, 658, 34, 25],
  bird3: [229, 710, 34, 25],
  bg1: [0, 0, 287, 512],
  bg2: [293, 0, 287, 512],
  logo1: [585, 181, 113, 98],
  logo2: [698, 181, 182, 49],
  fence1: [112, 646, 51, 320],
  fence2: [168, 646, 51, 320],
  di: [585, 0, 334, 113]
}
canvas.addEventListener('click', function (e) {
  if (game.ism)
    return
  if (game.isFinsh) {
    game.gameStart()
  } else {
    game.bird.flyUp();
  }
  e.defaultPrevented;
});
canvas.addEventListener('touchstart', function (e) {
  game.ism = true;
  if (game.isFinsh) {
    game.gameStart()
  } else {
    game.bird.flyUp();
  }
  e.defaultPrevented;
})
// canvas.addEventListener('touchstart', function (e) {
//   game.bird.flyUp();
//   e.defaultPrevented;
// }
// )
function Bird() {
  this.y = 50;//高度
  this.x = 50;//x
  this.size = 30;//高度
  this.isDie = true;//已撞墙
  this.thongh = false;//需要计算
  this.vel = 0;//速度
  this.g = 0.15//加速度
  this.src = null;//图片路径
}
Bird.prototype.flyUp = function () {
  if (!this.isDie) {
    this.vel = -5;
    if (this.y <= 0) {
      this.vel = 0
      this.y = 0
    }
  }
}
Bird.prototype.updated = function () {
  var fence = game.fenceList[0]
  this.y += this.vel;
  if (this.y <= 0) {
    this.vel = 0
    this.y = 0
  }
  this.vel += this.g;
  //沉底
  if (this.y > (api.getHeight() - 170)) {
    this.y = api.getHeight() - 170;
    this.isDie = true;
    game.isFinsh = true;
  }
  // 进入管道
  if (fence.x - this.x <= this.size && !fence.isAdd) {
    this.thongh = true;
    fence.isAdd = true;
  }
  //移出管道
  if ((this.x - fence.x >= fence.width) && fence.isAdd && this.thongh) {
    this.thongh = false;
    game.score++;
  }
  if (this.thongh && (this.y + this.size / api.bi('bird1') >= fence.y + 10 || this.y + 10 <= fence.y - fence.kong)) {
    // debugger;
    this.isDie = true;
  }
}
function ForxJs() {
  this.name = '刘焱';
  this.size = 100;
  this.xVel = (1 + Math.random() * 2);
  this.yVel = (1 + Math.random() * 2);
  this.x = Math.random() * api.getWidth();
  this.y = Math.random() * api.getHeight();
}
ForxJs.prototype.updata = function () {
  this.x += this.xVel;
  this.y += this.yVel;
  var maxh = api.getHeight(), maxw = api.getWidth() - this.size * 2;
  if (this.x > maxw  || this.x < 0 ) {
    this.xVel *= -1;
    this.x = this.x > maxw ? maxw : this.x;
  }
  if (this.y < this.size|| this.y > maxh){
    this.yVel *= -1;
    this.y = this.y < maxh ? this.y : maxh ;
  }
}
function Fence() {
  this.x = api.getWidth();
  this.y = Math.random() * (api.getHeight() - 200) + 50;
  this.velX = 1.5;
  this.kong = 150;
  this.width = 100;
  this.isAdd = false;
  this.jiange = 300
}
Fence.prototype.updated = function () {
  this.x -= this.velX;//移动
}

function Game() {
  this.bird = new Bird();
  this.fenceList = [];
  this.score = 0;
  this.forx = new ForxJs();
  this.isFinsh = true;
  this.canClick = false;
  this.bgx = 0;
  this.dix = 0;
  this.dih = 150;
  this.gameStart = function () {
    this.bird.isDie = false;
    this.isFinsh = false;
    this.bird.thongh = false;
    this.bird.y = 50;
    this.bird.vel = 0;
    this.fenceList.splice(0, this.fenceList.length);
    this.score = 0;
  }
  this.gameOver = function () {
    this.bird.thongh = false;
    this.isFinsh = true;
    this.bird.isDie = true;
    this.bird.y = 50;
    this.bird.vel = 0;
  }
}
var api = {
  getWidth: function () {
    return canvas.width
  },
  getHeight: function () {
    return canvas.height
  },
  setSize: function () {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
  },
  drawImg: function (name, x, y, w, h) {
    ctx.drawImage(img, atlas[name][0], atlas[name][1], atlas[name][2], atlas[name][3], x, y, w, h)
  },
  bi(name) {
    return atlas[name][2] / atlas[name][3]
  },
}
var game;
var img = new Image();
img.src = "./img/atlas.png";

img.onload = function () {
  api.setSize();
  game = new Game();
  loop();
}
// 绘制背景
Game.prototype.drawBg = function () {
  var bgx = 0;
  var bgbi = api.bi('bg1');
  while (bgx < (api.getWidth() + (api.getHeight() - game.dih) * bgbi)) {
    api.drawImg('bg1', bgx - this.bgx % (api.getHeight() - this.dih) * bgbi, 0, (api.getHeight() - this.dih) * bgbi, api.getHeight() - this.dih);
    bgx += (api.getHeight() - this.dih) * bgbi;
  }
  this.bgx += 0.5;
}
//绘制地皮
Game.prototype.drawDi = function () {
  var dix = 0, dibi = api.bi('di')
  while (dix < (api.getWidth() + this.dih * dibi)) {
    api.drawImg('di', dix - this.dix, api.getHeight() - this.dih, this.dih * dibi, 150);
    dix += 150 * dibi;
  }
  this.dix += 1;
  if (this.dix > 150 * dibi) {
    this.dix = 0
  }
}
//绘制障碍
Game.prototype.drawFen = function () {
  var febi = api.bi('fence2')
  var lastFence = game.fenceList[this.fenceList.length - 1];
  if (this.fenceList.length === 0 || lastFence.x < (api.getWidth() - this.fenceList[0].jiange)) {
    this.fenceList.push(new Fence())
  }
  if (this.fenceList[0].x < -this.fenceList[0].width) {
    this.fenceList.shift()
  }
  for (var i = 0; i < this.fenceList.length; i++) {
    var fence = this.fenceList[i];
    api.drawImg('fence2', fence.x, fence.y, fence.width, fence.width / febi)
    api.drawImg('fence1', fence.x, (fence.y - fence.width / febi) - fence.kong, fence.width, fence.width / febi);
    fence.updated();
  }
}
//绘制小鸟
Game.prototype.drawBi = function () {
  var bibi = api.bi('bird1')
  api.drawImg('bird1', this.bird.x, this.bird.y, this.bird.size, this.bird.size / bibi);
  this.bird.updated();
}
//绘制引导
Game.prototype.drawLogo = function () {
  var biLogo = api.bi('logo1');
  api.drawImg('logo1', api.getWidth() / 2 - 75, 200, 150, 150 / biLogo);
  var biLogo = api.bi('logo2');
  api.drawImg('logo2', api.getWidth() / 2 - 100, 100, 200, 200 / biLogo);

  // this.bird.updated();
}
// 绘制虚化
Game.prototype.drawMo = function () {
  ctx.beginPath()
  ctx.fillStyle = "rgba(255,255,255,0.5)";
  ctx.strokeStyle = "rgba(255,255,255,0.5)"
  ctx.fillRect(0, 0, api.getWidth(), api.getHeight())
  ctx.closePath()
}
//绘制分数
Game.prototype.drawScore = function () {
  ctx.save()
  ctx.fillStyle = "rgba(255,255,255)";
  ctx.strokeStyle = "rgba(0,0,0)"
  ctx.font = "48px serif";
  ctx.shadowColor = "#999999";
  ctx.shadowBlur = 10;
  ctx.fillText(this.score, 20, 50);
  // ctx.strokeText(this.forx.name, 20, 50);
  ctx.restore()
}

Game.prototype.drowName = function () {
  ctx.save()
  ctx.fillStyle = "rgba(255,255,255,0.3)";
  // ctx.strokeStyle = "rgba(0,0,0)"
  ctx.font = this.forx.size + "px 微软雅黑";
  // ctx.shadowColor = "#999999";
  // ctx.shadowBlur = 10;
  ctx.globalCompositeOperation ='lighten'
  ctx.fillText(this.forx.name, this.forx.x, this.forx.y);
  // ctx.strokeText(game.score, 20, 50);
  // console.log(this.forx.y);
  ctx.restore()
  this.forx.updata()
}
function loop() {
  game.drawBg();
  if (!game.isFinsh) {
    game.drawFen();
    game.drawBi();
  }
  game.drawDi();
  if (game.bird.isDie) {
    game.drawMo();
  }
  if (game.isFinsh) {
    game.drawLogo();
  }
  game.drawScore();
  game.drowName();
  window.requestAnimationFrame(loop)
}
window.onload = function () {
  api.setSize();
}
window.onresize = function () {
  api.setSize();
}